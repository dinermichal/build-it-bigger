package com.udacity.gradle.builditbigger;

import android.annotation.SuppressLint;
import android.os.AsyncTask;

import com.google.api.client.extensions.android.http.AndroidHttp;
import com.google.api.client.extensions.android.json.AndroidJsonFactory;
import com.google.api.client.googleapis.services.AbstractGoogleClientRequest;
import com.google.api.client.googleapis.services.GoogleClientRequestInitializer;
import com.udacity.gradle.builditbigger.backend.myApi.MyApi;

import java.io.IOException;

@SuppressLint("NewApi")
public class GCETask extends AsyncTask<Void, Void, String> {


    public final SimpleIdlingResource idlingResourceTask;

    public GCETask(SimpleIdlingResource idlingResource) {
        this.idlingResourceTask = idlingResource;
    }

    @Override
    protected String doInBackground(Void... params) {
        if (idlingResourceTask != null) {
            idlingResourceTask.setIdleState(false);
        }
            MyApi.Builder builder = new MyApi.Builder(AndroidHttp.newCompatibleTransport(),
                    new AndroidJsonFactory(), null)
                    .setRootUrl("http://10.0.2.2:8080/_ah/api/")
                    .setGoogleClientRequestInitializer(new GoogleClientRequestInitializer() {
                        @Override
                        public void initialize(AbstractGoogleClientRequest<?> abstractGoogleClientRequest) throws IOException {
                            abstractGoogleClientRequest.setDisableGZipContent(true);
                        }
                    });

            MyApi myApiService = builder.build();


        String joke = null;
        try {
            joke= myApiService.sayJokes().execute().getJokesFromEndPoint();
            return joke;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return joke;
    }
}
