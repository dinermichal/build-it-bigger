package com.udacity.gradle.builditbigger.free;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.udacity.gradle.builditbigger.GCETask;
import com.udacity.gradle.builditbigger.R;
import com.udacity.gradle.builditbigger.SimpleIdlingResource;

import static com.mike.displayjokes.JokesFragment.sendStrings;


/**
 * A placeholder fragment containing a simple view.
 */
public class MainActivityFragment extends Fragment {

    public MainActivityFragment() {
    }

    @SuppressLint("NewApi")
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_main, container, false);
        final SimpleIdlingResource idlingResource = (SimpleIdlingResource)((MainActivity)getActivity()).getIdlingResource();

        AdView mAdView = (AdView) root.findViewById(R.id.adView);
            AdRequest adRequest = new AdRequest.Builder()
                    .addTestDevice(AdRequest.DEVICE_ID_EMULATOR)
                    .build();
            mAdView.loadAd(adRequest);

        Button jokeButton = root.findViewById(R.id.tell_joke);
        jokeButton.setOnClickListener(new View.OnClickListener() {
            @SuppressLint("StaticFieldLeak")
            @Override
            public void onClick(View view) {
               new GCETask(idlingResource){
                   @Override
                   protected void onPostExecute(String strings) {
                       if(strings != null){
                           Intent intent = sendStrings(getContext(), strings);
                           startActivity(intent);
                       }
                       else{
                           Intent intent = sendStrings(getContext(), "Something wen't wrong");
                           startActivity(intent);
                       }
                       if (idlingResourceTask != null) {
                           idlingResource.setIdleState(true);
                       }
                   }
               }.execute();

            }
        });
        return root;
    }
}
