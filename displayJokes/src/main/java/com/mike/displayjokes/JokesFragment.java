package com.mike.displayjokes;

import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.Objects;

public class JokesFragment extends Fragment {

    public static final String JOKES = "jokes";
    private TextView mJokesTextView;

    public JokesFragment() {
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_jokes, container, false);
        String jokes = Objects.requireNonNull(getActivity()).getIntent().getStringExtra(JOKES);
        mJokesTextView = rootView.findViewById(R.id.jokesTv);
        mJokesTextView.setText(jokes);

        return rootView;
    }

    public static Intent sendStrings(Context context, String joke) {
        return new Intent(context, JokesActivity.class).putExtra(JOKES, joke);
    }
}
