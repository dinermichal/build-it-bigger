package com.mike.displayjokes;

import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;

public class JokesActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.container);
        FragmentManager fragmentManager = getSupportFragmentManager();
        if (savedInstanceState == null) {
            JokesFragment jokesFragment = new JokesFragment();
            fragmentManager.beginTransaction()
                    .add(R.id.fragment_container, jokesFragment)
                    .commit();
        }
    }
}
